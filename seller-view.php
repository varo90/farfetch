<?php
    include('header.php');
?>
    
    <div class="contenedor">
        <table id="data-info" class="display" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>ID Farfetch</th>
                    <th>Descuento %</th>
                    <th>Referencia</th>
                    <th>Imagen</th>
                    <th>Marca</th>
                    <th>Temporada</th>
                    <th>C&oacute;digos</th>
                    <th>Code</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>ID Farfetch</th>
                    <th>Descuento %</th>
                    <th>Referencia</th>
                    <th>Imagen</th>
                    <th>Marca</th>
                    <th>Temporada</th>
                    <th>C&oacute;digos</th>
                    <th>Code</th>
                </tr>
            </tfoot>
        </table>
        <br>
        <div id="updated"></div>
        <a id='see_sent' href="info-sent-view.php" class="btn btn-info btn-lg" role="button" aria-pressed="true">Ver linkados</a>
        <button id='sendinfo_button' type='button' class='btn btn-primary btn-lg'>Enviar datos</button>
    </div>

    <script type="text/javascript" language="javascript" class="init">
        $( document ).ready(function() {
            $('#data-info').dataTable({
                "bProcessing": true,
                "aaSorting": [[7,'desc']],
                "lengthMenu": [[10, 25, 50, 100, 150, 200, 500], [10, 25, 50, 100, 150, 200, 500]],
                "sAjaxSource": "get-data-seller.php",
                "aoColumns": [
                    { mData: 'id_link' },
                    { mData: 'discount' },
                    { mData: 'id_sap' },
                    { mData: 'imagen' },
                    { mData: 'id_marca' },
                    { mData: 'temporada' },
                    { mData: 'codigos' },
                    { mData: 'code' }
                    
                ]
            });   
            $("#sendinfo_button").on( "click", function() {
                var ids = new Array();
                $('#data-info').find(':input.textinput').each(function(){
                    var input = $(this).val();
                    if(input !== '') {
                        var id_link = $(this).val();
                        var discount = $(this).parent().parent().find(':nth-child(2)').find(':input.numinput').val();
                        var id_sap = $(this).parent().parent().find(':nth-child(3)').text();
                        ids.push({id_link:id_link, id_sap:id_sap, discount:discount});
                    }
                });
                $.ajax({
                    url: 'send_id_farfetch.php',
                    type: 'post',
                    contentType: 'application/x-www-form-urlencoded',
                    data: {ids:ids},
                    success: function( data ){
                        $('#updated').html( data );
                        $('#data-info').DataTable().ajax.reload();
                    }
                });
            });
        });
    </script>
</body>
</html>