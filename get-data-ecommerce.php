<?php

error_reporting(E_ALL);
ini_set('display_errors', 'On');
    date_default_timezone_set('Europe/Madrid');
    include('db_connections.php');
    
    $conn = mysql_connection('farfetch');
    
    $sql = "SELECT * FROM correspondencias ORDER BY `fecha-insertado`";
    
    $data = array();
    foreach ($conn->query($sql) as $row) {
        $id_link = $row['ID_Farfetch'];
        $id_sap = $row['ID_SAP'];
        $discount = $row['descuento'] . '%';
        $sent = $row['enviado'];
        $linked_by = $row['insertado-por'];
        $date_linked = date( "Y-m-d H:m:s", strtotime( $row['fecha-insertado'] ) );
        if($sent == 0) {
            $sent = '<div style="display: none;">3</div><div class="type_btn" style="display: none;">1</div><div class="sent" id="div-' . $id_link . '-' . $id_sap . '"><button id="' . $id_link . '-' . $id_sap . '" style="cursor: pointer" type="button" class="btn btn-info">Marcar como linkado</button></div>';
            if($row['enviado-por'] == '' || $row['enviado-por'] == null) {
                $sent_by = '---';
            } else {
                 $sent_by = $row['enviado-por'];
            }
            if($row['fecha-enviado'] == '0000-00-00 00:00:00' || $row['fecha-enviado'] == null) {
                $date_sent = '---';
            } else {
                 $date_sent = date( "Y-m-d H:m:s", strtotime( $row['fecha-enviado'] ) );
            }
        } else if($sent == 1) {
            $sent = '<div style="display: none;">5</div><div class="sent"><font color="green"><b>Procesado</b></font></div>';
            $sent_by = $row['enviado-por'];
            $date_sent = date( "Y-m-d H:m:s", strtotime( $row['fecha-enviado'] ) );
        } else if($sent == 2) {
            $sent = '<div style="display: none;">2</div><div class="type_btn" style="display: none;">1</div><div class="sent" id="div-' . $id_link . '-' . $id_sap . '"><button id="' . $id_link . '-' . $id_sap . '" style="cursor: pointer" type="button" class="btn btn-warning">Descuento editado</button></div>';
            $sent_by = $row['enviado-por'];
            $date_sent = date( "Y-m-d H:m:s", strtotime( $row['fecha-enviado'] ) );
        } else if($sent == 3) {
            $sent = '<div style="display: none;">1</div><div class="type_btn" style="display: none;">4</div><div class="sent" id="div-' . $id_link . '-' . $id_sap . '"><button id="' . $id_link . '-' . $id_sap . '" name="unlink" style="cursor: pointer" type="button" class="btn btn-danger">Deslinkado en Farfetch</button></div>';
            $sent_by = $row['enviado-por'];
            $date_sent = date( "Y-m-d H:m:s", strtotime( $row['fecha-enviado'] ) );
        } else if($sent == 4) {
            $sent = '<div style="display: none;">4</div><div class="sent"><font color="orange"><b>Inactivo</b></font></div>';
            $sent_by = $row['enviado-por'];
            $date_sent = date( "Y-m-d H:m:s", strtotime( $row['fecha-enviado'] ) );
        }
        $data[] = array('id_link'=>$id_link, 'id_sap'=>$id_sap, 'discount'=>$discount, 'sent'=>$sent, 'linked_by'=>$linked_by, 'date_linked'=>$date_linked, 'sent_by'=>$sent_by, 'date_sent'=>$date_sent);
    }
    
    disconnect($conn);

    $results = array(
        "sEcho" => 1,
        "iTotalRecords" => count($data),
        "iTotalDisplayRecords" => count($data),
        "aaData"=>$data
    );

    echo json_encode($results);