<?php

class queries {
    
    static function get_barcode($or_array) {
        $sql = "SELECT oi.ItemCode AS barcode, oi.U_GSP_REFERENCE as reference, tcco.U_GSP_Name as color, tcmsi.U_GSP_Desc as size
        FROM OITM oi WITH (NOLOCK)
            LEFT JOIN [dbo].[@GSP_TCSIZE] tcmsi WITH (NOLOCK) ON oi.U_GSP_Size = tcmsi.Code
            LEFT JOIN [dbo].[@GSP_TCCOLOR] tcco WITH (NOLOCK) ON oi.U_GSP_Color = tcco.Code
        WHERE $or_array";
        return $sql;
    }
    
    function join_conditions($filters,$sql_ref,$sql_col) {
        $texts = array();
        foreach($filters as $filter) {
            if($filter == null) {continue;}
            $texts[] = "($sql_ref='$filter->reference' AND $sql_col='$filter->color')";
        }
        $conditions = implode(' OR ', $texts);
        return $conditions;
    }
}
