<?php
    include('header.php');
?>

    <div class="contenedor">
        <table id="data-info" class="display" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>ID Farfetch</th>
                    <th>ID SAP</th>
                    <th>Descuento</th>
                    <th>Linked</th>
                    <th>Solicitado por</th>
                    <th>Fecha solicitado</th>
                    <th>Linkado por</th>
                    <th>Fecha Linkado</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>ID Farfetch</th>
                    <th>ID SAP</th>
                    <th>Descuento</th>
                    <th>Linked</th>
                    <th>Solicitado por</th>
                    <th>Fecha solicitado</th>
                    <th>Linkado por</th>
                    <th>Fecha Linkado</th>
                </tr>
            </tfoot>
        </table>
        <br>
        <div id="updated"></div>
        <center>
            <a id='upload-f-ff' href="upload-file-ff.php" class="btn btn-info btn-lg" role="button" aria-pressed="true">Generar fichero Barcodes...</a>
        </center>
    </div>

    <script type="text/javascript" language="javascript" class="init">
        $( document ).ready(function() {
            $('#data-info').dataTable({
                "bProcessing": true,
                "sAjaxSource": "get-data-ecommerce.php",
                "lengthMenu": [[10, 25, 50, 100, 150, 200, 500], [10, 25, 50, 100, 150, 200, 500]],
                "aaSorting": [[3,'asc'],[5,'desc']],
                "aoColumns": [
                    { mData: 'id_link' },
                    { mData: 'id_sap' },
                    { mData: 'discount' },
                    { mData: 'sent' },
                    { mData: 'linked_by' },
                    { mData: 'date_linked' },
                    { mData: 'sent_by' },
                    { mData: 'date_sent' }
                ]
            });   
            $('#data-info tbody').on( 'click', 'button', function () {
                var $row = $(this).parent().parent().parent();
                var id_link = $row.find('td:nth-child(1)').html();
                var id_sap = $row.find(':nth-child(2)').html();
                var button = $row.find(':nth-child(4) .type_btn').html();
                $.ajax({
                    url: 'mark_as_sent.php',
                    type: 'post',
                    contentType: 'application/x-www-form-urlencoded',
                    data: {id_link:id_link, id_sap:id_sap, button:button},
                    success: function( data ){
                        $('#div-'+id_link+'-'+id_sap).html( data );
                    }
                });
            } );
        });
    </script>
</body>
</html>