<?php

    include('db_connections.php');
    
    $sql = "SELECT tcm.Code as code, tcm.U_GSP_Picture as picture,
                   CONCAT(tcm.U_GSP_REFERENCE, CASE WHEN tcmco.U_GSP_NAME IS NULL THEN '' ELSE CONCAT('-',tcmco.U_GSP_NAME) END) AS id_sap,
                   omrc.FirmName as marca,
                   tcm.U_GSP_SeasonCode as season,
                   CONCAT(tcmca.U_GSP_CATALOGNUM,CASE WHEN tcmca.U_GSP_MATERIALDESC IS NULL THEN '' ELSE CONCAT(' - ',tcmca.U_GSP_MATERIALDESC) END,
                   CASE WHEN tcmca.U_GSP_COLORDESC IS NULL THEN '' ELSE CONCAT('<br>',tcmca.U_GSP_COLORDESC) END) as codes
            FROM [dbo].[@GSP_TCMODELPROPERS] tcmp with (NOLOCK)
                LEFT JOIN [dbo].[@GSP_TCMODEL] tcm with (NOLOCK) ON tcmp.U_GSP_MODELCODE = tcm.Code
                LEFT JOIN [dbo].[@GSP_TCCOLOR] tcmco with (NOLOCK) ON tcmp.U_GSP_MODELCOLOR = tcmco.Code 
                LEFT JOIN [dbo].[@GSP_TCMODELCATALOG] tcmca with (NOLOCK) ON tcmp.U_GSP_MODELCODE = tcmca.U_GSP_MODELCODE and tcmca.U_GSP_MODELCOLOR = tcmp.U_GSP_MODELCOLOR
                LEFT JOIN [dbo].[OMRC] omrc with (NOLOCK) ON tcm.U_GSP_FirmCode = omrc.FirmCode 
            WHERE U_GSP_PROPERTYVALUE = 'Y'";
    
    $ms_conn = mssql_connection_test();
    $query = $ms_conn->prepare($sql);
    $query->execute();
    $items = $query->fetch();
    disconnect($ms_conn);
    
    $conn = mysql_connection('farfetch');
    $query_mysql = $conn->prepare("SELECT * FROM correspondencias WHERE ID_SAP=:sap LIMIT 1");
    $query_mysql->bindParam(':sap', $id_sap);
    
    $data = array();
    foreach($items as $row) {
        $code = $row['code'];
        $id_sap = $row['id_sap'];
        $pic = $row['picture'];
        $img_url = '..\Fotos_Sap\\' . $pic;
        $imagen = "<a href='$img_url' target='_blank' class='lightbox_trigger'><img src='$img_url' alt='No image' height='60' width='60'></a>";
        $temporada = $row['season'];
        $marca = utf8_decode($row['marca']);
        $codes = utf8_decode($row['codes']);
        $query_mysql->execute();
        if($query_mysql->rowCount() == 0) {
            $input_frftch = "<input class='textinput' type='number' name='$id_sap' value=''>";
            $input_discount = "<input class='numinput' type='number' name='$id_sap-dis' value='0' step='1' min='0' max='100'>";
            $data[] = array('id_link'=>$input_frftch, 'discount'=>$input_discount, 'id_sap'=>$id_sap, 'imagen'=>$imagen, 'id_marca'=>$marca, 'temporada'=>$temporada, 'codigos'=>$codes, 'code'=>$code);
        }
    }
    
    disconnect($conn);

    $results = array(
        "sEcho" => 1,
        "iTotalRecords" => count($data),
        "iTotalDisplayRecords" => count($data),
        "aaData"=>$data
    );

    echo json_encode($results);