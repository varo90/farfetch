<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
    include('db_connections_new.php');
    include('queries.php');
    
    $uploaded_file = $_GET['fich'];
    
    read_items_in_file($uploaded_file);
    
    function read_items_in_file($nom_fich) {
        $extension = get_file_extension($nom_fich);
        if($extension == 'xlsx') {
            process_xlsx($nom_fich);
        } else if($extension == 'csv') {
            process_csv($nom_fich);
        } else {
            die();
        }
    }
    
    function get_data($or_array) {
        $db_ms = new db();
        $results = $db_ms->make_query(queries::get_barcode($or_array),[],PDO::FETCH_OBJ);
        unset($db_ms);
        return $results;
    }
    
    //NO USAR///////////////////////////////
    function process_xlsx($nom_fich) {
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $nom_fich . '"');
        header('Cache-Control: max-age=0');
        include('../UTILS/PHPExcel/Classes/PHPExcel.php');
        include('../UTILS/PHPExcel/Classes/PHPExcel/Writer/Excel2007.php');
        
        $file = '/var/www/farfetch/uploaded/'.$nom_fich;

        $excelReader = PHPExcel_IOFactory::createReaderForFile($file);
        $objPHPExcel = $excelReader->load($file);
        $worksheet = $objPHPExcel->getSheet(0);
        $index_worksheet = $objPHPExcel->setActiveSheetIndex(0);

        $highestRow = $index_worksheet->getHighestRow(); 

        for ($row = 1; $row <= $highestRow; $row++) {
            $item_size = $worksheet->getCell("H$row")->getValue();
            if($item_size != 'XXXS' && $item_size != 'XXS' && $item_size != 'XS' && $item_size != 'XXXL' && $item_size != 'XXXXL') {
                $item_id = $worksheet->getCell("G$row")->getValue();
                if($item_id != 'SKU' && $item_id != '' && $item_id != NULL) {
                    $brcd = get_data($item_id,$item_size);
                    $barcode = '#' . $brcd;
                    $index_worksheet->setCellValue("J$row", $barcode);
                }
            }
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        
        $objPHPExcel->disconnectWorksheets();
        unset($objPHPExcel);
    }
    //NO USAR///////////////////////////////
    
    function process_csv($nom_fich) {
        $itms = file('uploaded/'.$nom_fich, FILE_IGNORE_NEW_LINES);
        $rows_max = 10000;
        $num_rows = count($itms);
        if($num_rows <= $rows_max) {
            $rows_to_read = $num_rows-1;
        } else {
            $rows_to_read = $rows_max;
        }
        //
        $conditions = get_cond_sql($itms,$rows_to_read);
        $results = get_data($conditions);
        write_csv($results,$nom_fich,$rows_to_read);
        //
    }
    
    function get_cond_sql($itms,$rows_to_read) {
        $or_array = '';
        foreach($itms as $cont => $row) {
            $item_data = str_getcsv($row, ";"); //parse the items in rows 
            $item_id = $item_data[6];
            if($item_id != 'SKU' && $item_id != '' && $item_id != NULL) {
                $itm_id = explode(' ', $item_id);
                $sap_id = $itm_id[0];
                $itm_color = $itm_id[1];
                $item_size = $item_data[7];
                $or_array = $or_array . "(oi.U_GSP_REFERENCE = '$sap_id' AND tcco.U_GSP_Name = '$itm_color' AND tcmsi.U_GSP_Desc = '$item_size')";
                if(/*$cont > 0 && */$cont < $rows_to_read) {
                    $or_array = $or_array . ' OR ';
                }
            }
            if ($cont == $rows_to_read) { break; }
        }
        return $or_array;
    }
    
    function write_csv($results,$nom_fich,$rows_to_read) {
        $itms_new = file('uploaded/'.$nom_fich, FILE_IGNORE_NEW_LINES);
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=barcodes.csv');
        $fp = fopen('php://output', 'w');
        foreach($itms_new as $cont => $row_csv) {
            $item_data = str_getcsv($row_csv, ";"); //parse the items in rows 
            print_barcode($fp,$results,$item_data); // Set varcode in proper column
            if ($cont == $rows_to_read) { break; }
        }
        fclose($fp);
    }
    
    function print_barcode($fp,$results,$item_data) {
        $ref_csv = $item_data[6];
        $size_csv = $item_data[7];
        if($ref_csv != 'SKU' && $ref_csv != '' && $ref_csv != NULL) {
            // Get match
            foreach($results as $row_sql) {
                $barcode = '#' . $row_sql['barcode'];
                $ref_sql = $row_sql['reference'] . ' ' . $row_sql['color'];
                $size_sql = $row_sql['size'];
                if($ref_csv == $ref_sql && $size_csv == $size_sql) {
                    $item_data[9] = $barcode;
                    fputcsv($fp, $item_data,';');
                    return true;
                }
            }
        }
        return false;
    }
    
    function get_file_extension($nom_fich) {
        $n_fich = explode('.', $nom_fich);
        return end($n_fich);
    }