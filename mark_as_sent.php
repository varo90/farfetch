<?php
    include('db_connections.php');
    include('session_init.php');

    $conn = mysql_connection('farfetch');
    
    // Prepare query and bind variables
    $query = $conn->prepare("UPDATE correspondencias SET enviado=:sent, `enviado-por`=:sentby, `fecha-enviado`=:datesent WHERE ID_Farfetch=:farfetch AND ID_SAP=:sap");
    $query->bindParam(':farfetch', $id_link, PDO::PARAM_STR);
    $query->bindParam(':sap', $id_sap, PDO::PARAM_STR);
    $query->bindParam(':sent', $sent, PDO::PARAM_STR);
    $query->bindParam(':sentby', $user, PDO::PARAM_STR);
    $query->bindParam(':datesent', $date, PDO::PARAM_STR);

    $id_link = $_POST['id_link'];
    $id_sap = $_POST['id_sap'];
    $sent = $_POST['button'];
    $user = $_SESSION['username_link'];
    $date = date("Y-m-d H:i:s");
    
    try {
        if($query->execute()) {
            echo '<font color="green"><b>Registro enviado</b></font>';
        }
    }
    catch (PDOException $e) {
        echo 'No se pudo actualizar el registro: ' . $id_link . ' , ' . $id_sap . '. ' . $e->getMessage() . '<br>';
    }
    
    disconnect($conn);