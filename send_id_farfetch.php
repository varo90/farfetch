<?php
    include('db_connections.php');
    include('session_init.php');

    $conn = mysql_connection();

    $ids = $_POST['ids'];
    
    // Prepare query and bind variables
    $query = $conn->prepare("INSERT INTO correspondencias (ID_farfetch, ID_SAP, descuento, enviado, `insertado-por`, `fecha-insertado`) VALUES (:farfetch, :sap, :discount, :enviado, :insby, :dateins)");
    $query->bindParam(':farfetch', $id_link);
    $query->bindParam(':sap', $id_sap);
    $query->bindParam(':discount', $discount);
    $query->bindParam(':enviado', $sent);
    $query->bindParam(':insby', $user);
    $query->bindParam(':dateins', $date);
    
    $sent = 0;
    $user = $_SESSION['username_link'];
    $date = date('Y-m-d H:i:s');
    
    foreach($ids as $cont => $id) {
        $id_link = $id['id_link'];
        $id_sap = $id['id_sap'];
        $discount = $id['discount'];
        try {
            $query->execute();
        }
        catch (PDOException $e) {
            echo 'No se pudo introducir uno de los registros: ' . $id_link . ' , ' . $id_sap . '. ' . $e->getMessage() . ' - ' . $date . ' - ' . $user . '<br>';
        }
    }
    echo '<font color="green"><b>Registros introducidos.</b></font>';
    
    disconnect($conn);
    
?>
